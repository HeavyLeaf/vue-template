module.exports = {
  singleQuote: true, // 单引号
  bracketSpacing: true, // 对象大括号直接是否有空格，默认为true，效果：{ foo: bar }
  semi: false, // 行位是否使用分号
  tabWidth: 2,
  trailingComma: 'none', // 是否使用尾逗号
  printWidth: 100 // 一行的字符数，如果超过会进行换行
}
