module.exports = {
  devServer: {
    open: true,
    port: 8010,
    proxy: {
      ['/api']: {
        target: process.env.VUE_API_HOST,
        changeOrigin: true,
        pathRewrite: { ['^/api']: '/api' }
      }
    }
  },
  configureWebpack: {
    externals: {
      // cdn 配置
      // vue: 'Vue',
      // 'element-plus': 'ElementPlus'
    }
  },

  chainWebpack: (config) => {
    // 打包分析
    if (process.env.VUE_USE_ANALYZER === 'true') {
      // 分析
      config
        .plugin('webpack-bundle-analyzer')
        .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
    }
  }
}
