import http from './src/http'
import { handleUrls } from './src/http/handleUrls'

const requestPlugin = {
  install(app) {
    app.config.globalProperties.$http = http
  }
}

export { requestPlugin as default, handleUrls, http }
