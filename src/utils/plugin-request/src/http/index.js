import to from '@/utils/await-to'

import { Interceptors } from './interceptors'

export class HttpServer {
  constructor() {
    this.axiosInstance = new Interceptors({
      baseURL: '/',
      timeout: 60000,
      headers: {
        'Referrer-Policy': 'origin'
      }
    }).getInterceptors()
  }

  /**
   * 封装请求方法
   */
  request(config) {
    return to(
      new Promise((resolve, reject) => {
        this.axiosInstance(config)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            console.log(err)
            reject(err)
          })
      })
    )
  }
}

const http = new HttpServer()

export default http
