/**
 * 拦截器配置文件
 */

import axios from 'axios'
import { ElMessage } from 'element-plus'

export class Interceptors {
  constructor(config) {
    this.instance = axios.create(config)
    this.init()
  }

  init() {
    /* 请求拦截器 */
    this.instance.interceptors.request.use(
      (config) => {
        return config
      },
      (err) => {
        console.error(err)
      }
    )

    /* 响应拦截器 */
    this.instance.interceptors.response.use(
      async (response) => {
        let { data: res } = response
        const { status, msg } = res
        switch (status) {
          case 0:
            return res
          default:
            ElMessage({
              message: msg,
              type: 'error',
              duration: 3 * 1000
            })
        }
      },
      (error) => {
        ElMessage({
          message: error.message,
          type: 'error',
          duration: 3 * 1000
        })
        return Promise.reject(error)
      }
    )
  }

  getInterceptors() {
    return this.instance
  }
}
