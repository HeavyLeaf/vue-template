import http from './index'
/**
 * 将请求函数名与对应url的map生成对应函数
 * @param urls {Object} url集合，{
                                   postUrls: {
                                      'name': 'url'
                                   },
                                   postFormUrls: {
                                      'name': 'url'
                                   },
                                   getUrls: {
                                      'name': 'url'
                                   },
                                   putUrls: {
                                      'name': 'url'
                                   },
                                   deleteUrls: {
                                      'name': 'url'
                                   }
                                }
 * @return {{}}
 */
export const handleUrls = (urls = {}) => {
  const apis = {}
  // post
  if (urls.postUrls) {
    let postUrls = urls.postUrls
    Object.keys(postUrls).map((item) => {
      apis[item] = (data, config, id) => {
        return http.request({
          url: id ? `${postUrls[item]}/${id}` : postUrls[item],
          method: 'post',
          data,
          ...config
        })
      }
    })
  }
  // post Content-Type = application/x-www-form-urlencoded
  if (urls.postFormUrls) {
    let postUrls = urls.postFormUrls
    Object.keys(postUrls).map((item) => {
      apis[item] = (data = {}, config, id) => {
        return http.request({
          url: id ? `${postUrls[item]}/${id}` : postUrls[item],
          method: 'post',
          transformRequest: [
            function (oldData) {
              let newStr = ''
              for (let key in oldData) {
                if (oldData[key] != null) {
                  newStr += encodeURIComponent(key) + '=' + encodeURIComponent(oldData[key]) + '&'
                }
              }
              newStr = newStr.slice(0, -1)
              return newStr
            }
          ],
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data,
          ...config
        })
      }
    })
  }
  // get
  if (urls.getUrls) {
    let getUrls = urls.getUrls
    Object.keys(getUrls).map((item) => {
      apis[item] = (params, config, id) => {
        return http.request({
          url: id ? `${getUrls[item]}/${id}` : getUrls[item],
          method: 'get',
          params,
          ...config
        })
      }
    })
  }
  // put
  if (urls.putUrls) {
    let putUrls = urls.putUrls
    Object.keys(putUrls).map((item) => {
      apis[item] = (data, config, id) => {
        return http.request({
          url: id ? `${putUrls[item]}/${id}` : putUrls[item],
          method: 'put',
          data,
          ...config
        })
      }
    })
  }
  // delete
  if (urls.deleteUrls) {
    let deleteUrls = urls.deleteUrls
    Object.keys(deleteUrls).map((item) => {
      apis[item] = (data, config, id) => {
        return http.request({
          url: id ? `${deleteUrls[item]}/${id}` : deleteUrls[item],
          method: 'delete',
          data,
          ...config
        })
      }
    })
  }

  return apis
}
