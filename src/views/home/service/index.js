import { handleUrls } from '@/utils/plugin-request'

const urls = {
  postUrls: {},
  postFormUrls: {},
  getUrls: {},
  putUrls: {},
  deleteUrls: {}
}

const api = handleUrls(urls)

export default api
