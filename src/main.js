import { createApp, version } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import router from './router'
import store from './store'
import 'element-plus/lib/theme-chalk/index.css'
import requestPlugin from '@/utils/plugin-request'

const app = createApp(App)

console.log('==========================================')
console.log('vue version:', version)
console.log('element-plus version:', ElementPlus.version)
console.log('==========================================')

app.use(store)
app.use(router)
app.use(ElementPlus)
app.use(requestPlugin)
app.mount('#app')
