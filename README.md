# template

特点：
- vue3
- vuex
- vue-router
- webpack
- axios
- element-plus
- less
- vuepress
- eslint js语法校验
- stylelint 样式校验
- prettier 代码格式检查
- ls-lint 文件命名检查
- 将比较大的包从打包中抽离，减小 chunk-vendors.js 文件体积，有效提高加载与首屏渲染速度。
- 生产环境下移除 console.log() [在 babel.config.js 中配置]


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
