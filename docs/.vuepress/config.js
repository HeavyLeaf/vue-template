module.exports = {
  title: "Hello VuePress",
  description: "Just playing around",
  themeConfig: {
    // 默认搜索框配置
    searchMaxSuggestions: {
      search: false,
      searchMaxSuggestions: 10
    },
    // 最后提交时间
    lastUpdated: "最后更新时间",
    // 页面滚动效果
    smoothScroll: true,
    // 头部导航配置
    nav: [
      { text: "首页", link: "/" },
      { text: "指南", link: "/guide/" }
    ],
    // 侧边导航，采用分组形式
    sidebarDepth: 3, // 嵌套的标题链接，深度取到h3
    sidebar: {
      '/guide/':[{
        title: "指南",  // 分组标题
        children: [
          { path: "/guide/", title: "指南" },
          { path: "/guide/page1", title: "目录1" }
        ]
      }]
    }
  }


};
